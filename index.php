<?php

$list = explode('/', trim($_SERVER['REQUEST_URI'], '/'));

$line = [];
foreach ($list as $value) {
    if($value == 'public') break;
    $line[] = $value;
}

if(!empty($line)) {
    $url = '/' . implode('/', $line) . '/public/';
} else {
    $url = '/public/';
}

header("Location: " . $url);

exit;
