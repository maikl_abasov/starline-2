
<page-toolbar
        title="Главная"
        :list="[{ title: 'Главная'}]">
    <template v-slot:actions >
        <?php if(!empty($_COOKIE['auth_state'])) { ?>
            <a href="logout" class="btn btn-sm btn-primary"  id="kt_toolbar_primary_button" > Выход </a>
        <?php } ?>
    </template>
</page-toolbar>

<!--begin::Content-->
<div class="post d-flex flex-column-fluid" id="kt_post">

    <div id="kt_content_container" class="container-fluid">

    <div class="card">

        <!--begin::Card header-->
        <div class="card-header border-0 pt-6">
            Home header
        </div>

        <!--begin::Card body-->
        <div class="card-body pt-0">
            Home content
        </div>
        <!--end::Card body -->

    </div>

</div>

</div>
<!--end::Content-->




