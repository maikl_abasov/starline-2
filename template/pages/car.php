
<page-toolbar
   title="Авто"
   :list="[{ title: 'Главная', url: '/'}, { title: 'Авто'} ]">
    <template v-slot:actions >
        <?php if(!empty($_COOKIE['auth_state'])) { ?>
            <a href="logout" class="btn btn-sm btn-primary"  id="kt_toolbar_primary_button" > Выход </a>
        <?php } ?>
    </template>
</page-toolbar>


<!--begin::Content-->
<div class="post d-flex flex-column-fluid" >

    <cars-page />

</div>
<!--end::Content-->




