
<div class="w-lg-500px bg-white rounded shadow-sm p-10 p-lg-15 mx-auto">

    <form method="post" action="user-register" class="form w-100 fv-plugins-bootstrap5 fv-plugins-framework">

        <div class="text-center mb-10">
            <h1 class="text-dark mb-3"> Регистрация </h1>
        </div>

        <div class="fv-row mb-10 fv-plugins-icon-container">
            <label class="form-label fs-6 fw-bolder text-dark"> Имя </label>
            <input class="form-control form-control-lg form-control-solid" value=""
                   type="text" name="username" autocomplete="off">
            <div class="fv-plugins-message-container invalid-feedback"></div>
        </div>

        <div class="fv-row mb-10 fv-plugins-icon-container">
            <label class="form-label fs-6 fw-bolder text-dark">Email</label>
            <input class="form-control form-control-lg form-control-solid" value=""
                   type="text" name="email" autocomplete="off">
            <div class="fv-plugins-message-container invalid-feedback"></div>
        </div>

        <div class="fv-row mb-10 fv-plugins-icon-container">
            <label class="form-label fs-6 fw-bolder text-dark">Телефон</label>
            <input class="form-control form-control-lg form-control-solid" value=""
                   type="text" name="phone" autocomplete="off">
            <div class="fv-plugins-message-container invalid-feedback"></div>
        </div>

        <div class="fv-row mb-10 fv-plugins-icon-container">
            <div class="d-flex flex-stack mb-2">
                <label class="form-label fw-bolder text-dark fs-6 mb-0">Пароль</label>
            </div>
            <input class="form-control form-control-lg form-control-solid"
                   value="" type="text" name="password" autocomplete="off">
            <div class="fv-plugins-message-container invalid-feedback"></div>
        </div>

        <div class="fv-row mb-10 fv-plugins-icon-container">
            <div class="d-flex flex-stack mb-2">
                <label class="form-label fw-bolder text-dark fs-6 mb-0">Повторите пароль</label>
            </div>
            <input class="form-control form-control-lg form-control-solid"
                   value="" type="text" name="confirm_password" autocomplete="off">
            <div class="fv-plugins-message-container invalid-feedback"></div>
        </div>

        <div class="text-center">

            <button type="submit" id="kt_sign_in_submit" class="btn btn-lg btn-primary w-100 mb-5">
                <span class="indicator-label"> Зарегистрироваться </span>
                <span class="indicator-progress">Please wait...
				<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
            </button>

        </div>

    </form>

</div>
