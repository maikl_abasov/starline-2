
const BaseMixin = {

    data() {
        return {
            pageData: PageData,
            siteUri: SiteUri,
            templateUrl: TemplateUrl,
            metronicUrl: MetronicUrl,
            itemsList: [],

            topMenu : [
                { url: SiteUri + '/', title: 'Главная'},
                { url: 'user', title: 'Профиль'},
                { url: 'car', title: 'Авто'},

                { url: 'login-form', title: 'Логин'},
                { url: 'register-form', title: 'Регистрация'},
            ],
        }
    },

    methods: {

        async send(url, data = null, method = 'get') {
            const response = (data) ? await axios[method](url, data) :
                                      await axios[method](url);
            return response.data;
        },

        getPageData(key) {
            return (this.pageData[key]) ? this.pageData[key] : this.pageData;
        },

        searchItems(table, field, value, limit = 10) {
            const url  = 'search/items';
            const data = { table, field, value, limit };
            return this.send(url, data, 'post');
        },

        elemToggle(selector, hideElem = null) {
           let display = $(selector).css("display");
           if(hideElem) $(hideElem).hide();
           if(display == 'none') $(selector).show();
           else $(selector).hide();
        },

        getStore(name = null) {
            if(name) {
                if(this.$root.STORE[name]) {
                    return this.$root.STORE[name];
                }
            }
            return this.$root.STORE;
        },

        setStore(name, data) {
            let result = null;
            if(name) {
                if(this.$root.STORE[name]) {
                    this.$root.STORE[name] = data;
                    result = this.$root.STORE[name];
                }
            }
            return result;
        },

    },

}