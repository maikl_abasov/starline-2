const Cars = {
    data() {
        return {
           liveSearchState: false,
           pageCount: 0,
           itemsCount: 0,
           form: {
               page: { page_num: 1, limit: 6 },
               sort: {},
               like: {},
               between: {},
               in: {},
           },
           carItems: [],
           sqlTest: null,
        }
    },

    created() {
        this.getCarItems();
    },

    methods: {

        getCarItems() {
            const data = this.form;
            const url = 'car/get-items';
            this.send(url, data, 'post').then(response => {
                const { count, list } = response;
                this.carItems = list;
                this.itemsCount = count;
                this.pageCount = Math.ceil((parseInt(this.itemsCount) / parseInt(this.form.page.limit)));
                this.sqlTest = response.sql;
                // console.log(this.sqlTest);
            })
        },

        setFilter(data) {
           this.form.like[data.field] = data.value;
           this.getCarItems();
        },

        inSetFilter(data) {
            this.form.in[data.field] = data.list;
            this.getCarItems();
        },

        betweenFilter(data) {
            this.form.between[data.field] = data.list;
            this.getCarItems();
        },

        searchFieldItems(field, value) {
            const url = 'material/field/search-items';
            this.send(url, { field, value }).then(response => {
                $("#datalist-name").show();
                this.fieldItems = response;
            })
        },

        setName(item) {
            this.form.like['name'] = item.name;
            $("#datalist-name").hide();
        },

        clearAllFilters() {
            this.form = {
                page: { page_num: 1, limit: 6 },
                sort: {},
                like: {},
                between: {},
                in: {},
            };
            this.getCarItems();
        },

        liveSearch() {
            if(!this.liveSearchState) return;
            this.getCarItems();
        },

        pageNext(pageNum) {
            this.form.page.page_num = pageNum;
            this.getCarItems();
        },
    },

    template: "#cars-page-template-container",
}
