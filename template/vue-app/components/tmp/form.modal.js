
const FormModal = {

    emits: ['set_filter'],
    props: ['fname', 'title'],

    data() {
        return {
            list: [],
        }
    },

    created() {
        this.init();
    },

    methods: {

        init() {
            let list = this.getList();
            if(list && list.length) this.list = list;
            else this.addItem();
        },

        emitOk() {

            let fname = this.fname;
            let list  = [];
            let like  = [];
            for (let i in this.list) {
                let item = this.list[i];
                if(item.with) {
                    list.push(item);
                }
            }

            if(list.length > 0)  {
                $("#btn-" + this.fname).addClass("green-btn");
            } else {
                $("#btn-" + this.fname).removeClass("green-btn");
            }

            this.$emit('set_filter', { list , fname, like } );
        },

        emitClear() {
            this.list = [];
            this.list.push({ with: '', by: '' });
        },

        emitCancel() {
        },

        addItem() {
            this.list.push({ with: '', by: '' });
        },

        getList() {
            return this.$root.form.between[this.fname]
        },

    },

    template: `

     <div id="kt_content_container" className="container">
    <!--begin::Card-->
    <div className="card">
        <!--begin::Card body-->
        <div className="card-body p-0">
            <!--begin::Heading-->
            <div className="card-px text-center py-20 my-10">
                <!--begin::Title-->
                <h2 className="fs-2x fw-bolder mb-10">New Address Modal Example</h2>
                <!--end::Title-->
                <!--begin::Description-->
                <p className="text-gray-400 fs-5 fw-bold mb-13">Click on the below buttons to launch
                    <br/>a new address example.</p>
                <!--end::Description-->
                <!--begin::Action-->
                <a href="#" className="btn btn-primary er fs-6 px-8 py-4" data-bs-toggle="modal"
                   data-bs-target="#kt_modal_new_address">Add New Address</a>
                <!--end::Action-->
            </div>
            <!--end::Heading-->
            <!--begin::Illustration-->
            <div className="text-center px-5">
                <img src="assets/media/illustrations/statistics.png" alt="" className="mw-100 mh-300px"/>
            </div>
            <!--end::Illustration-->
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->
    <!--begin::Modal - New Address-->
    <div className="modal fade" id="kt_modal_new_address" tabIndex="-1" aria-hidden="true">
        <!--begin::Modal dialog-->
        <div className="modal-dialog modal-dialog-centered mw-650px">
            <!--begin::Modal content-->
            <div className="modal-content">
                <!--begin::Form-->
                <form className="form" action="#" id="kt_modal_new_address_form">
                    <!--begin::Modal header-->
                    <div className="modal-header" id="kt_modal_new_address_header">
                        <!--begin::Modal title-->
                        <h2>Add New Address</h2>
                        <!--end::Modal title-->
                        <!--begin::Close-->
                        <div className="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                            <!--begin::Svg Icon | path: icons/duotone/Navigation/Close.svg-->
                            <span className="svg-icon svg-icon-1">
															<svg xmlns="http://www.w3.org/2000/svg"
                                                                 xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                                 height="24px" viewBox="0 0 24 24" version="1.1">
																<g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)"
                                                                   fill="#000000">
																	<rect fill="#000000" x="0" y="7" width="16"
                                                                          height="2" rx="1"/>
																	<rect fill="#000000" opacity="0.5"
                                                                          transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000)"
                                                                          x="0" y="7" width="16" height="2" rx="1"/>
																</g>
															</svg>
														</span>
                            <!--end::Svg Icon-->
                        </div>
                        <!--end::Close-->
                    </div>
                    <!--end::Modal header-->
                    <!--begin::Modal body-->
                    <div className="modal-body py-10 px-lg-17">
                        <!--begin::Scroll-->
                        <div className="scroll-y me-n7 pe-7" id="kt_modal_new_address_scroll" data-kt-scroll="true"
                             data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto"
                             data-kt-scroll-dependencies="#kt_modal_new_address_header"
                             data-kt-scroll-wrappers="#kt_modal_new_address_scroll" data-kt-scroll-offset="300px">
                            <!--begin::Notice-->
                            <!--begin::Notice-->
                            <div
                                className="notice d-flex bg-light-warning rounded border-warning border border-dashed mb-9 p-6">
                                <!--begin::Icon-->
                                <!--begin::Svg Icon | path: icons/duotone/Code/Warning-1-circle.svg-->
                                <span className="svg-icon svg-icon-2tx svg-icon-warning me-4">
																<svg xmlns="http://www.w3.org/2000/svg" width="24px"
                                                                     height="24px" viewBox="0 0 24 24" version="1.1">
																	<circle fill="#000000" opacity="0.3" cx="12" cy="12"
                                                                            r="10"/>
																	<rect fill="#000000" x="11" y="7" width="2"
                                                                          height="8" rx="1"/>
																	<rect fill="#000000" x="11" y="16" width="2"
                                                                          height="2" rx="1"/>
																</svg>
															</span>
                                <!--end::Svg Icon-->
                                <!--end::Icon-->
                                <!--begin::Wrapper-->
                                <div className="d-flex flex-stack flex-grow-1">
                                    <!--begin::Content-->
                                    <div className="fw-bold">
                                        <h4 className="text-gray-800 fw-bolder">Warning</h4>
                                        <div className="fs-6 text-gray-600">Updating address may affter to your
                                            <a href="#">Tax Location</a></div>
                                    </div>
                                    <!--end::Content-->
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <!--end::Notice-->
                            <!--end::Notice-->
                            <!--begin::Input group-->
                            <div className="row mb-5">
                                <!--begin::Col-->
                                <div className="col-md-6 fv-row">
                                    <!--begin::Label-->
                                    <label className="required fs-5 fw-bold mb-2">First name</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <input type="text" className="form-control form-control-solid" placeholder=""
                                           name="first-name"/>
                                    <!--end::Input-->
                                </div>
                                <!--end::Col-->
                                <!--begin::Col-->
                                <div className="col-md-6 fv-row">
                                    <!--end::Label-->
                                    <label className="required fs-5 fw-bold mb-2">Last name</label>
                                    <!--end::Label-->
                                    <!--end::Input-->
                                    <input type="text" className="form-control form-control-solid" placeholder=""
                                           name="last-name"/>
                                    <!--end::Input-->
                                </div>
                                <!--end::Col-->
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div className="d-flex flex-column mb-5 fv-row">
                                <!--begin::Label-->
                                <label className="d-flex align-items-center fs-5 fw-bold mb-2">
                                    <span className="required">Country</span>
                                    <i className="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip"
                                       title="Your payment statements may very based on selected country"></i>
                                </label>
                                <!--end::Label-->
                                <!--begin::Select-->
                                <select name="country" data-control="select2"
                                        data-dropdown-parent="#kt_modal_new_address"
                                        data-placeholder="Select a Country..."
                                        className="form-select form-select-solid">
                                    <option value="">Select a Country...</option>
                                    <option value="AF">Afghanistan</option>
                                    <option value="AX">Aland Islands</option>
                                    <option value="AL">Albania</option>
                                    <option value="DZ">Algeria</option>
                                    <option value="AS">American Samoa</option>
                                    <option value="AD">Andorra</option>
                                    <option value="AO">Angola</option>
                                    <option value="AI">Anguilla</option>
                                    <option value="AQ">Antarctica</option>
                                    <option value="AG">Antigua and Barbuda</option>
                                    <option value="AR">Argentina</option>
                                    <option value="AM">Armenia</option>

                                </select>
                                <!--end::Select-->
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div className="d-flex flex-column mb-5 fv-row">
                                <!--begin::Label-->
                                <label className="required fs-5 fw-bold mb-2">Address Line 1</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <input className="form-control form-control-solid" placeholder="" name="address1"/>
                                <!--end::Input-->
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div className="d-flex flex-column mb-5 fv-row">
                                <!--begin::Label-->
                                <label className="required fs-5 fw-bold mb-2">Address Line 2</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <input className="form-control form-control-solid" placeholder="" name="address2"/>
                                <!--end::Input-->
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div className="d-flex flex-column mb-5 fv-row">
                                <!--begin::Label-->
                                <label className="fs-5 fw-bold mb-2">Town</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <input className="form-control form-control-solid" placeholder="" name="city"/>
                                <!--end::Input-->
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div className="row g-9 mb-5">
                                <!--begin::Col-->
                                <div className="col-md-6 fv-row">
                                    <!--begin::Label-->
                                    <label className="fs-5 fw-bold mb-2">State / Province</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <input className="form-control form-control-solid" placeholder="" name="state"/>
                                    <!--end::Input-->
                                </div>
                                <!--end::Col-->
                                <!--begin::Col-->
                                <div className="col-md-6 fv-row">
                                    <!--begin::Label-->
                                    <label className="fs-5 fw-bold mb-2">Post Code</label>
                                    <!--end::Label-->
                                    <!--begin::Input-->
                                    <input className="form-control form-control-solid" placeholder="" name="postcode"/>
                                    <!--end::Input-->
                                </div>
                                <!--end::Col-->
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div className="fv-row mb-5">
                                <!--begin::Wrapper-->
                                <div className="d-flex flex-stack">
                                    <!--begin::Label-->
                                    <div className="me-5">
                                        <!--begin::Label-->
                                        <label className="fs-5 fw-bold">Use as a billing adderess?</label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <div className="fs-7 fw-bold text-gray-400">If you need more info, please check
                                            budget planning
                                        </div>
                                        <!--end::Input-->
                                    </div>
                                    <!--end::Label-->
                                    <!--begin::Switch-->
                                    <label className="form-check form-switch form-check-custom form-check-solid">
                                        <!--begin::Input-->
                                        <input className="form-check-input" name="billing" type="checkbox" value="1"
                                               checked="checked"/>
                                        <!--end::Input-->
                                        <!--begin::Label-->
                                        <span className="form-check-label fw-bold text-gray-400">Yes</span>
                                        <!--end::Label-->
                                    </label>
                                    <!--end::Switch-->
                                </div>
                                <!--begin::Wrapper-->
                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end::Scroll-->
                    </div>
                    <!--end::Modal body-->
                    <!--begin::Modal footer-->
                    <div className="modal-footer flex-center">
                        <!--begin::Button-->
                        <button type="reset" id="kt_modal_new_address_cancel" className="btn btn-white me-3">Discard
                        </button>
                        <!--end::Button-->
                        <!--begin::Button-->
                        <button type="submit" id="kt_modal_new_address_submit" className="btn btn-primary">
                            <span className="indicator-label">Submit</span>
                            <span className="indicator-progress">Please wait...
														<span
                                                            className="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                        </button>
                        <!--end::Button-->
                    </div>
                    <!--end::Modal footer-->
                </form>
                <!--end::Form-->
            </div>
        </div>
    </div>
    <!--end::Modal - New Address-->
</div>

    `,
}

