
const BaseMixin = {

    methods: {
        async send(url, data = null) {
            const response = (data) ?
                await axios.post(url, data) : await axios.get(url);
            return response.data;
        },
    }
}

const appConfig = {
    data() {
        return {
            liveSearchState: false,
            itemsCount: 0,
            fieldItems: [],
            materialItems: [],
            pageCount: 0,
            form: {
                like: {
                    number: null,
                    created_dt: null,
                    group: '',
                    user: '',
                    vendor: '',
                    name: ''
                },

                between: {
                    number: null,
                    created_dt: null,
                },
                page: { page_num: 1, limit: 10 },
            }
        }
    },

    created() {
        this.getMaterialItems();
    },

    methods: {

        getMaterialItems() {
            const url = 'material/search';
            this.send(url, this.form).then(response => {
                const { count, list } = response;
                this.materialItems = list;
                this.itemsCount = count[0]['count(id)'];
                this.pageCount = Math.ceil((parseInt(this.itemsCount) / parseInt(this.form.page.limit)));
            })
        },

        setFilter(data) {
            this.form.like[data.fname] = '';
            this.form.between[data.fname] = data.list;
            this.getMaterialItems();
        },

        searchFieldItems(field, value) {
            const url = 'material/field/search-items';
            this.send(url, { field, value }).then(response => {
                $("#datalist-name").show();
                this.fieldItems = response;
            })
        },

        setName(item) {
            this.form.like['name'] = item.name;
            $("#datalist-name").hide();
        },

        clearAllFilters() {

            const like = {
                number: null,
                created_dt: null,
                group: '',
                user: '',
                vendor: '',
                name: ''
            };

            const between = {
                number: null,
                created_dt: null,
            };

            const page = { page_num: 1, limit: 10 };

            this.form = { like, between, page}

            this.getMaterialItems();
        },

        liveSearch() {
            if(!this.liveSearchState) return;
            this.getMaterialItems();
        },

        pageNext(pageNum) {
            this.form.page.page_num = pageNum;
            this.getMaterialItems();
        }

    },
}

const {createApp} = Vue;
const app = createApp(appConfig);
app.mixin(BaseMixin);
app.component('form-filter', FormFilter);
app.component('auth-form', AuthForm);
app.mount('#app');