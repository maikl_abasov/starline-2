const BetweenFilter = {

    emits: ['between_filter'],
    props: ['table', 'field', 'title', 'from', 'field_name'],
    data() {
        return {
            list: [],
            textContent: '',
        }
    },

    created() {
        this.add();
    },

    computed: {
        getList() {
            return this.list;
        },
    },

    methods: {

        filterEmit() {

            let field = this.field_name;

            let list = [];

            for (let i in this.list) {
                let item = this.list[i];
                if (item.with && item.by) {
                    list.push(item)
                }
            }

            this.list = list;

            this.close();

            this.$emit('between_filter', {list, field});
        },

        clear() {
            this.list = [];
            this.list.push({with: '', by: ''});
        },

        add() {
            this.list.push({with: '', by: ''});
        },

        close() {
            this.elemToggle('between-filter__' + this.field + this.table, '.in-filter-container');
        },

        pasteСlipboard() {
            window.navigator.clipboard.readText()
                .then(text => {
                    console.log(text);
                    this.clipboardDataRender(text)
                }).catch(err => {
                console.log('Something went wrong', err);
            });
        },

        clipboardDataRender(content) {
            let list = content.split("\n");
            for (let i in list) {
                let line = list[i];
                let item = line.split("\t");
                if (item[0] && item[1]) {
                    this.list.push({with: item[0], by: item[1]});
                }
            }
        },

    },

    template: `

        <a @click.prevent="elemToggle('#between-filter__' + field + table, '.in-filter-container')" href="#" 
          class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1">
               <span class="svg-icon svg-icon-3">
                 <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"></rect>
                        <path d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z" fill="#000000"></path>
                        <path d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z" fill="#000000" opacity="0.3"></path>
                    </g>
                 </svg>
             </span>
        </a>
    
        <div  :id="'between-filter__' + field + table" class="menu menu-sub menu-sub-dropdown p-7 w-325px w-md-375px in-filter-container" 
              style="" >

            <form data-kt-search-element="advanced-options-form" class="pt-1">
            
                <h3 class="fw-bold text-dark mb-7"> {{title}} </h3>
                
                <div class="mb-5">
<!--                    <pre>{{list}}</pre>-->
                    <div class="separator"></div>
<!--                    <div id="fg">rt</div>-->
                </div>

                <!--begin::Input group-->
                <div class="row mb-8">

                    <div class="col-6"> С </div>
                    <div class="col-6"> По </div>

                    <template v-for="(item) in getList">
                        <div class="col-6" style="margin-bottom: 4px">
                            <input v-model="item.with" type="text"  class="form-control form-control-sm form-control-solid" 
                            placeholder="с" >
                        </div>
                        
                        <div class="col-6">
                            <input v-model="item.by" type="text"  class="form-control form-control-sm form-control-solid" 
                            placeholder="По" >
                        </div>
                    </template>
                    
<!--                    <div class="col-12">-->
<!--                         <textarea v-model="textContent" @input="pasteFromСlipboard($event)"-->
<!--                             cols="2" rows="2" class="form-control" style="width: 100%;"></textarea>-->
<!--&lt;!&ndash;                         <pre>{{textContent}}</pre>    &ndash;&gt;-->
<!--                    </div>-->

                </div>
                <!--end::Input group-->
                
                <!--begin::Actions-->
                <div class="d-flex justify-content-end">
                
                   <button @click.prevent="close()"  class="btn btn-sm btn-white fw-bolder btn-active-light-primary me-2" > 
                        <span class="svg-icon svg-icon-1" style="cursor: pointer; padding-top: 7px; text-align: center !important;">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <rect opacity="0.5" x="7.05025" y="15.5356" width="12" height="2" rx="1" transform="rotate(-45 7.05025 15.5356)" fill="currentColor"/>
                              <rect x="8.46447" y="7.05029" width="12" height="2" rx="1" transform="rotate(45 8.46447 7.05029)" fill="currentColor"/>
                            </svg>
                        </span>
                   </button>
                   
                   <button @click.prevent="clear()" type="reset" class="btn btn-sm btn-white fw-bolder btn-active-light-primary me-2" > 
                        <span class="svg-icon svg-icon-2" style="cursor: pointer; padding-top: 7px; text-align: center !important;">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" fill="currentColor"/>
                            <path opacity="0.5" d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" fill="currentColor"/>
                            <path opacity="0.5" d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" fill="currentColor"/>
                        </svg></span>
                   </button>
                   
                   <button @click.prevent="add()" class="btn btn-sm btn-white fw-bolder btn-active-light-primary me-2" > 
                       <span class="svg-icon svg-icon-1" style="cursor: pointer; padding-top: 7px; text-align: center !important;">
                           <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <rect opacity="0.5" x="11" y="18" width="12" height="2" rx="1" transform="rotate(-90 11 18)" fill="currentColor"/>
                              <rect x="6" y="11" width="12" height="2" rx="1" fill="currentColor"/>
                            </svg>
                        </span>
                    </button>

                    <a @click.prevent="filterEmit()" href="#" class="btn btn-sm fw-bolder btn-primary"  >
                        Выполнить
                    </a>
                    
                </div>
                <!--end::Actions-->
                
                <div class="d-flex justify-content-end" style="margin-top: 10px"> 
                    <a @click.prevent="pasteСlipboard()" href="#" class="btn btn-sm fw-bolder btn-primary"  >
                        Загрузить из буфера
                    </a>
                </div>
                
            </form>
            
        </div>
    `,
}