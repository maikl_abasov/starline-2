
const CarFilter = {
    emits: ['set_filter'],
    props: ['field', 'title'],
    data() {
        return {
            value: '',
        }
    },

    created() {
    },

    methods: {
        search() {
           const field = this.field;
           const value = this.value;
           this.$emit('set_filter', { field, value});
        }
    },

    template: `
      <div class="text-gray-800 text-hover-primary mb-1" 
          style="display: flex; border: 0px red solid" >
            <input v-model="value" @input="search()" 
                   type="text"  :placeholder="title"
                   class="form-control form-control-lg form-control-solid mb-1 mb-lg-0" >
      </div>
    `,
}
