const InFilter = {

    emits: ['in_filter'],
    props: ['table', 'field', 'title', 'from', 'field_name'],
    data() {
        return {
            searchValue: '',
            list: [],
            realList: [],
        }
    },

    created() {},

    computed: {

        getList() {
            return this.list;
        },

        getRealList() {
            return this.realList;
        },
    },

    methods: {

        filterEmit() {

            let field = this.field_name;

            for(let i in this.list) {
                let item = this.list[i];
                if(!item.state) continue;
                let value = item.value;
                //list.push(item.value);
                let empty = true;
                for(let i in this.realList) {
                    if(this.realList[i].value == value) {
                        empty = false;
                        break;
                    }
                }

                if(empty) this.realList.push(item);
            }

            let list = [];
            for(let i in this.realList) {
                let item = this.realList[i];
                if(!item.state) continue;
                list.push(item.value);
            }

            this.list = [];
            this.searchValue = '';

            this.$emit('in_set_filter', { list, field } );
        },

        async search() {

            let limit = 10;
            let table = this.table;
            let field = this.field;
            let value = this.searchValue;

            const response = await this.searchItems(table, field, value, limit);

            this.list = response.map((item) => {
                let value = item[field];
                return {state: false, value}
            });

        },

        init() {
            let list = [];
            if (this.form && this.form[this.field_name]) {
                let items = this.form[this.field_name];
                list = items.map((value) => {
                    return {state: true, value}
                })
            }
            return list;
        },

        deleteRealList(index) {
            if(!this.realList[index].state) {
                delete this.realList[index];
            }
        },

        close() {
            this.elemToggle('#in-filter__' + this.field + this.table, '.in-filter-container');
        },
    },

    template: `

      <a @click.prevent="elemToggle('#in-filter__' + field + table, '.in-filter-container')" href="#" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1">
           <span class="svg-icon svg-icon-3">
             <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="currentColor"/>
                <path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="currentColor"/>
             </svg>
         </span>
      </a>
  
      <div :id="'in-filter__' + field + table" 
           class="menu menu-sub menu-sub-dropdown menu-column w-350px w-lg-375px in-filter-container"
           style="" >

            <div class="d-flex flex-column bgi-no-repeat rounded-top"
                 style="background-image:url('https://21090.aqq.ru/GLOBAL-ASSETS/metronic/assets/media/misc/pattern-1.jpg')">
               
                <h5 class="text-white fw-bold px-2 mt-3 mb-3">
                    {{title}} <span class="fs-8 opacity-75 ps-3"></span>
                </h5>

            </div>
            <!--end::Heading-->
            
            <!--begin::Tab content-->
            <div class="tab-content">
            
               <div class="m-5">
                    <input v-model="searchValue" @input="search()"
                           type="text" class="form-control form-control-sm form-control-solid"   placeholder="" >
               </div>
            
                <!--begin::Tab panel-->
                <div class="tab-pane fade active show" id="kt_topbar_notifications_1" role="tabpanel">
                
                
<!--                    <pre>{{realList}}</pre>-->

                    <!--begin::Items-->
                    <div class="scroll-y mh-325px my-5 px-8">
                      
                       <template v-for="(item, index) in getRealList">
                            <div class="d-flex flex-stack py-4"><div class="d-flex align-items-center">

                                <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                    <input @click="deleteRealList(index)" v-model="item.state" class="form-check-input" type="checkbox" >
                                </div>
   
                                <div class="mb-0 me-2"><a href="#" class="fs-6 text-gray-800 text-hover-primary fw-bolder">
                                     {{item.value}}
                                </a></div>
                                  
                            </div></div>
                        </template>
                      
                      
                        <template v-for="(item) in getList">
                            <!--begin::Item-->
                            <div class="d-flex flex-stack py-4">  
                                <div class="d-flex align-items-center">
                                   
<!--                                    <div class="symbol symbol-35px me-4">-->
<!--                                        <span class="svg-icon svg-icon-2 svg-icon-primary">-->
<!--                                            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">-->
<!--                                               <path d="M11.2600599,5.81393408 L2,16 L22,16 L12.7399401,5.81393408 C12.3684331,5.40527646 11.7359848,5.37515988 11.3273272,5.7466668 C11.3038503,5.7680094 11.2814025,5.79045722 11.2600599,5.81393408 Z" fill="#000000" opacity="0.3"></path>-->
<!--                                               <path d="M12.0056789,15.7116802 L20.2805786,6.85290308 C20.6575758,6.44930487 21.2903735,6.42774054 21.6939717,6.8047378 C21.8964274,6.9938498 22.0113578,7.25847607 22.0113578,7.535517 L22.0113578,20 L16.0113578,20 L2,20 L2,7.535517 C2,7.25847607 2.11493033,6.9938498 2.31738608,6.8047378 C2.72098429,6.42774054 3.35378194,6.44930487 3.7307792,6.85290308 L12.0056789,15.7116802 Z" fill="#000000"></path>-->
<!--                                            </svg>-->
<!--                                        </span>-->
<!--                                    </div>-->
                                    
                                    <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                        <input v-model="item.state" class="form-check-input" type="checkbox" >
                                    </div>
       
                                    <div class="mb-0 me-2">
                                        <a href="#" class="fs-6 text-gray-800 text-hover-primary fw-bolder">
                                            {{item.value}}
                                        </a>
    <!--                                    <div class="text-gray-400 fs-7">Confidential staff documents</div>-->
                                    </div>
                                  
                                </div>
                                
    <!--                            <span class="badge badge-light fs-8">2 hrs</span>-->
                               
                            </div>
                            <!--end::Item-->
                        </template>

                    </div>
                    <!--end::Items-->
                    
                    <!--begin::View more-->
                    <div class="py-1 text-center border-top">
      
                        <div class="text-center1 m-3">
                        
                             <button @click="elemToggle('#in-filter__' + field)" type="reset" 
                                     class="btn btn-sm btn-white fw-bolder btn-active-light-primary me-2" > Отмена </button>
                        
                             <a @click.prevent="filterEmit(); elemToggle('#in-filter__' + field)" href="#" 
                                    class="btn btn-sm btn-primary px-6" > Применить </a>
                        </div>

<!--                         <div class="d-flex justify-content-end pt-7">-->
<!--                             <button type="reset" class="btn btn-sm btn-white fw-bolder btn-active-light-primary me-2" >  Cancel</button>-->
<!--                             <button type="submit" class="btn btn-sm fw-bolder btn-primary">Save Changes</button>-->
<!--                         </div>-->
                       
                    </div>
                    <!--end::View more-->
                </div>
                <!--end::Tab panel-->

            </div>
            <!--end::Tab content-->
        
     </div>
          
    `,
}


