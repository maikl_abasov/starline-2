
const Toolbar = {
    props:["title", "list"],
    data() { return {} },
    methods: {},

    template: `
        <div class="toolbar" >
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
        
                <div class="page-title d-flex align-items-center me-3 flex-wrap mb-5 mb-lg-0 lh-1">
                    <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3"> {{title}} </h1>
                    <span class="h-20px border-gray-200 border-start mx-4"></span>
                    
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                    
                        <template v-for="(item) in list" >
                        
                           <template v-if="item?.url">
                                <li class="breadcrumb-item text-muted">
                                   <a :href="item.url" class="text-muted text-hover-primary">{{item.title}}</a>
                                </li>
                                <li class="breadcrumb-item">
                                   <span class="bullet bg-gray-200 w-5px h-2px"></span>
                                </li>
                           </template>
                           <template v-else >
                                <li class="breadcrumb-item text-dark"> {{item.title}} </li>
                           </template>
                            
                        </template>

                    </ul>
                    <!--end::Breadcrumb-->
                </div>
        
                <!--begin::Actions-->
                <div class="d-flex align-items-center py-1">
                    <slot name="actions"></slot>
                </div>
                <!--end::Actions-->
        
            </div>
        </div>
    `,
}