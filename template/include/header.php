
<div id="kt_header" style="" class="header align-items-stretch">
    <!--begin::Container-->
    <div class="container-fluid d-flex align-items-stretch justify-content-between">

        <!-- Логотип -->
        <div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0 me-lg-15">
            <a href="/public">
                <img alt="Logo" src="<?php echo METRONIC_URL;?>/assets/media/logos/logo-default.svg" class="h-20px" />
            </a>
        </div>
        <!-- ./ Логотип-->

        <div class="d-flex align-items-stretch justify-content-between flex-lg-grow-1">

            <!-- Меню -->
            <div class="d-flex align-items-stretch" id="kt_header_nav">
                <div class="header-menu align-items-stretch" >

                    <div class="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold my-5 my-lg-0 align-items-stretch"
                         id="#kt_header_menu" >

                        <template v-for="(item) in topMenu">
                            <div class="menu-item me-lg-1">
                                <a :href="item.url" class="menu-link py-3" >
                                    <span class="menu-title"> {{item.title}} </span>
                                </a>
                            </div>
                        </template>

                    </div>

                </div>
            </div>
            <!-- ./ Меню -->

            <div class="d-flex align-items-stretch flex-shrink-0" >
                <div class="topbar d-flex align-items-stretch flex-shrink-0">

                    <!--begin::User-->
                    <div class="d-flex align-items-stretch" id="kt_header_user_menu_toggle">

                        <!-- Фото пользователя -->
                        <div class="topbar-item cursor-pointer symbol px-3 px-lg-5 me-n3 me-lg-n5 symbol-30px symbol-md-35px" >
                            <img src="<?php echo METRONIC_URL; ?>/assets/media/avatars/150-2.jpg" alt="metronic" />
                        </div>
                        <!-- ./ Фото пользователя -->

                        <!-- Вертикальное меню-->
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold py-4 fs-6 w-275px">

                            <!--begin::Menu item-->
                            <div class="menu-item px-5" style="border:1px red solid; display: block" >

                                <!--begin::Menu sub-->
                                <div class="menu-sub menu-sub-dropdown w-175px py-4"
                                     style="display: block" >

                                    <div class="menu-item px-3">
                                        <a href="account/referrals.html" class="menu-link px-5">Referrals</a>
                                    </div>

                                    <div class="menu-item px-3">
                                        <a href="account/billing.html" class="menu-link px-5">Billing</a>
                                    </div>

                                    <div class="menu-item px-3">
                                        <a href="account/statements.html" class="menu-link px-5">Payments</a>
                                    </div>

                                    <div class="menu-item px-3">
                                        <a href="account/statements.html" class="menu-link d-flex flex-stack px-5">Statements
                                            <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="View your statements"></i></a>
                                    </div>

                                    <div class="separator my-2"></div>

                                    <div class="menu-item px-3">
                                        <div class="menu-content px-3">
                                            <label class="form-check form-switch form-check-custom form-check-solid">
                                                <input class="form-check-input w-30px h-20px" type="checkbox" value="1" checked="checked" name="notifications" />
                                                <span class="form-check-label text-muted fs-7">Notifications</span>
                                            </label>
                                        </div>
                                    </div>
                                    <!--end::Menu item-->
                                </div>
                                <!--end::Menu sub-->
                            </div>
                            <!--end::Menu item-->

                        </div>
                        <!-- ./ Вертикальное меню-->

                    </div>
                    <!--end::User -->

                    <!--begin::Header menu toggle-->
                    <div class="d-flex align-items-stretch d-lg-none px-3 me-n3" title="Show header menu">
                        <div class="topbar-item" id="kt_header_menu_mobile_toggle">
                            <i class="bi bi-text-left fs-1"></i>
                        </div>
                    </div>
                    <!--end::Header menu toggle-->

                </div>
            </div>

        </div>

    </div>
    <!--end::Container-->
</div>
