<?php

$pageData = json_encode($this->data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

?>

<!doctype html>
<html>
<head>

    <meta charset="utf-8">
    <meta content="width=device-width,initial-scale=1,minimal-ui" name="viewport">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />

    <link rel="stylesheet" href="<?php echo METRONIC_URL;?>/assets/plugins/global/plugins.bundle.css">
    <link rel="stylesheet" href="<?php echo METRONIC_URL;?>/assets/css/style.bundle.css">

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.4/dist/jquery.min.js"></script>
    <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <link rel="stylesheet" href="<?php echo TEMPLATE_URL;?>/assets/css/style.css">

    <style></style>

    <script>

        const PageData = <?php echo $pageData; ?>;
        const SiteUri = '<?php echo SITE_URI; ?>';
        const TemplateUrl = '<?php echo TEMPLATE_URL;?>';
        const MetronicUrl = '<?php echo METRONIC_URL;?>';

    </script>

    <style>
        .table th {
            vertical-align: top !important;
        }
    </style>

</head>

<template id="cars-page-template-container">
    <?php include_once APP_PATH . '/template/vue-app/templates/cars.html'; ?>
</template>

<body id="kt_body"
      class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed toolbar-tablet-and-mobile-fixed"
      style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px" >

<div id="app">
    <div class="d-flex flex-column flex-root"><div class="page d-flex flex-row flex-column-fluid"><div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">

            <!--begin::Header-->
            <?php include $this->templatePath . '/include/header.php'; ?>
            <!--end::Header-->

            <!--begin::Content-->
            <div class="content d-flex flex-column flex-column-fluid" id="kt_content">

                <?php
                    $templateFile =  $this->templatePath . '/pages/' . $this->file . '.php';
                    if(file_exists($templateFile)) {
                        include $templateFile;
                    } else {
                        include $this->templatePath . '/include/404.php';
                    }
                ?>

            </div>
            <!--end::Content-->

            <!--begin::Footer -->
            <?php include $this->templatePath . '/include/footer.php'; ?>
            <!--end::Footer -->

    </div></div></div>
</div>

<script src="<?php echo METRONIC_URL;?>/assets/plugins/global/plugins.bundle.js"></script>
<script src="<?php echo METRONIC_URL;?>/assets/js/scripts.bundle.js"></script>

<script src="<?php echo METRONIC_URL;?>/assets/js/custom/widgets.js"></script>
<script src="<?php echo METRONIC_URL;?>/assets/js/custom/apps/chat/chat.js"></script>
<script src="<?php echo METRONIC_URL;?>/assets/js/custom/modals/create-app.js"></script>
<script src="<?php echo METRONIC_URL;?>/assets/js/custom/modals/upgrade-plan.js"></script>

<!--############# ---->
<!---- VUE JS APP ---->
<script src="<?php echo TEMPLATE_URL;?>/vue-app/components/toolbar.js"></script>
<script src="<?php echo TEMPLATE_URL;?>/vue-app/components/in.filter.js"></script>
<script src="<?php echo TEMPLATE_URL;?>/vue-app/components/car.filter.js"></script>
<script src="<?php echo TEMPLATE_URL;?>/vue-app/components/between.filter.js"></script>
<script src="<?php echo TEMPLATE_URL;?>/vue-app/components/datalist.search.js"></script>
<script src="<?php echo TEMPLATE_URL;?>/vue-app/components/data.list.js"></script>

<script src="<?php echo TEMPLATE_URL;?>/vue-app/pages/cars.js"></script>

<script src="<?php echo TEMPLATE_URL;?>/vue-app/mixin.js"></script>
<script src="<?php echo TEMPLATE_URL;?>/vue-app/main.js"></script>

<script>

    const {createApp} = Vue;
    const app = createApp(appConfig);
    app.mixin(BaseMixin);

    // --- components ---
    app.component('page-toolbar', Toolbar);
    app.component('in-filter', InFilter);
    app.component('car-filter', CarFilter);
    app.component('between-filter', BetweenFilter);
    app.component('data-list', DataList);
    app.component('datalist-search', DatalistSearch)

    // --- pages ----
    app.component('cars-page', Cars);

    app.config.errorHandler = (err) => {
       console.log('ERROR-HANDLE', err)
    }

    app.mount('#app');

</script>

</body>
</html>