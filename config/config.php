<?php

use Dzion\Kernel\Interfaces\DatabaseInterface;
use Dzion\Kernel\Database\Database;

use Dzion\Kernel\Interfaces\QueryBuilderInterface;
use Dzion\Kernel\Database\QueryBuilder;

$CONFIG = getEnvFile();

return [

    'db_config' => [
        'host'   => getConfig($CONFIG, 'DB_HOST'),
        'driver' => getConfig($CONFIG, 'DB_CONNECTION'),
        'dbname' => getConfig($CONFIG, 'DB_DATABASE'),
        'user'   => getConfig($CONFIG, 'DB_USERNAME'),
        'password' => getConfig($CONFIG, 'DB_PASSWORD'),
        'port'   => getConfig($CONFIG, 'DB_PORT'),
    ],

    DatabaseInterface::class => Database::class,
    QueryBuilderInterface::class => QueryBuilder::class,

];