<?php

use Dzion\App\Controllers\HomeController;
use Dzion\App\Controllers\CarController;
use Dzion\App\Controllers\UserController;
use Dzion\App\Controllers\Auth\AuthController;
use Dzion\App\Models\User;
use Dzion\Kernel\Template;

$router = new Dzion\Kernel\Router\Router();

$router->middleware('GET|POST', ['/car', '/user'], function() {
    $auth = new AuthController(new User);
    $auth->check();
});

$router->get('/', [HomeController::class, 'index']);
$router->post('/search/items', [HomeController::class, 'searchItems']);
// $router->get('/home-hello/{name}/{message}', [HomeController::class, 'hello']);

$router->group('/car', function() use ($router) {

    $router->get('/', [CarController::class, 'index']);

    $router->post('/get-items', function () {
        (new CarController(new \Dzion\App\Models\Car))->getCarItems();
    });

    $router->get('/test', function () {
        $form = getTestFormData();
        (new CarController(new \Dzion\App\Models\Car))->getCarItems($form);
    });

});

// ---- AUTH ROUTES ----
$router->get('/register-form', function () {
    $template = new Template(TEMPLATE_PATH, 'index');
    $template->render('register', [])->view();
});

$router->post('/user-register', function () {

    $auth = new AuthController(new User);
    $state = $auth->register($_POST);
    $page = ($state) ? '/login-form' : '/register-form';
    $auth->redirect($page);

});

$router->get('/login-form', function () {
    $template = new Template(TEMPLATE_PATH, 'index');
    $template->render('auth', [])->view();
});

$router->post('/user-auth', function () {

    $auth = new AuthController(new User);
    $state = $auth->auth($_POST);
    $page = ($state) ? '/' : '/login-form';
    $auth->redirect($page);

});

$router->get('/logout', function () {

    $auth = new AuthController(new User);
    $auth->logout();
    $auth->redirect('/login-form');

});

// ---- ./ AUTH ROUTES ----

$router->get('/user', [UserController::class, 'index']);

$router->set404(function() {
    echo 'Такая страница не найдена!';
});

return $router;

////////////////////
///
///

function getTestFormData() {
    return [

        'page'    => ['page_num' => 1, 'limit' => 6],

        'sort'    => [
            'id' => 'DESC',
            'year' => 'ASC',
        ],

        'like'    => [
            'mark.name'  => 'r',
            'model.name' => 'ce',
        ],

        'between' => [
            'start_price' => [
                ['with' => 320000, 'by' => 379000],
                ['with' => 450000, 'by' => 570000],
            ],

            'year' => [
                ['with' => 2009, 'by' => 2011],
                ['with' => 2008, 'by' => ''],
            ],
        ],

        'in' => [
            'body_type' => ['Фургон', 'Универсал'],
        ],

    ];
}

/******************

$GET = [

    '/' => [HomeController::class, "index"],
    '/home-hello/{name}/{message}' => [HomeController::class, "hello"],

//    '/' => function () use ($database) {
//
//        $appController = new AppController($database);
//        $appController->checkAuth();
//        $data = $appController->getMaterials();
//        $template = new Template(TEMPLATE_PATH);
//        $template->render('home', $data)->getContent();
//    },

    '/logout' => function () use ($database) {
        setcookie("auth_state", "", time() - 60);
        (new AppController($database))->redirect('login-form');
    },

    '/login-form' => function () use ($database) {
        $template = new Template(TEMPLATE_PATH);
        $template->render('auth', [])->getContent();
    },

    '/material' => function () use ($database) {
        $appController = new AppController($database);
        $appController->checkAuth();
        $data = $appController->getMaterials();
        $template = new Template(TEMPLATE_PATH);
        $template->render('home', $data)->getContent();
    },

    '/material/export' => function () use ($database) {
        $data = (new AppController($database))->saveData(APP_PATH . '/public/export.csv');
        lg($data);
    },

    '/material/test' => function () use ($database) {
        $form = getForm();
        $data = (new AppController($database))->getMaterialSearch($form);
        lg($data);
    }

];

$POST = [
    '/user-auth' => function () use ($database) {
        $state = (new AppController($database))->auth($_POST['email'], $_POST['password']);
        $template = new Template(TEMPLATE_PATH);
        $template->render('auth', [$state])->getContent();
    },

    '/material/search' =>  function () use ($database) {
        $appController = new AppController($database);
        $appController->checkAuth();
        $form = $appController->post();
        $data = $appController->getMaterialSearch($form);
        (new Template(TEMPLATE_PATH))->json($data);
    },

    '/material/field/search-items' => function () use ($database) {
        $appController = new AppController($database);
        $appController->checkAuth();
        $form = $appController->post();
        $data = $appController->materialSearchFieldItems($form['field'], $form['value']);
        (new Template(TEMPLATE_PATH))->json($data);
    },

];

$PUT    = [];
$DELETE = [];
$PATCH  = [];

return [
    'get' => $GET,
    'post' => $POST,
    'put' => $PUT,
    'delete' => $DELETE,
    'patch' => $PATCH,
];
 *
******/

/*******

 * Варианты использования роутера

 * вариант 1
$router->get('/test-page/{name}/{age}', [B::class, "send"]);

 * вариант 2
$router->get('/test-page/{name}/{age}', B::class . "@" . "send");

 *  вариант 3
$router->get('/test-page/{name}/{age}', function ($name, $age) use ($database) {
    echo 'меня зовут ' . $name . ', мне ' . $age . ' лет';
});

 ****/


