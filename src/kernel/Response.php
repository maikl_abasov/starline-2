<?php

namespace Dzion\Kernel;

use Dzion\Kernel\Interfaces\ResponseInterface;

class Response implements ResponseInterface
{
    protected array | string $content;
    protected array | string $data;
    protected int $code;
    protected array $error;

    public function __construct(array | string $data, int $code = 200, array $error = []){
        $this->data = $data;
        $this->code = $code;
        $this->error = $error;
    }

    public function getContent() {
        echo $this->content; exit();
    }

    public function json() : self {
        $this->content = json_encode($this->data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        return $this;
    }

    public function html() : self {
        $this->content = $this->data;
        return $this;
    }

    public function getCode() : int{
        return $this->code;
    }

    public function getError() : array {
        return $this->error;
    }
}