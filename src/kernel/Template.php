<?php

namespace Dzion\Kernel;

use Dzion\Kernel\Interfaces\TemplateInterface;

class Template implements TemplateInterface
{
    protected $templatePath;
    protected $indexFile;
    protected $content;
    protected $data;
    protected $file;

    public function __construct(string $templatePath, $indexFile = 'index') {
        $this->templatePath = $templatePath;
        $this->indexFile = $indexFile;
    }

    public function render(string $file, array $data = []) : self {
        $this->file = $file;
        $this->data = $data;
        extract($data);
        ob_start();

        include $this->templatePath . '/' . $this->indexFile . '.php';

        $this->content = ob_get_contents();
        ob_get_clean();
        return $this;
    }

    public function getHtml() {
        return $this->content;
    }

    public function view() {
        echo $this->content;
    }

}