<?php

namespace Dzion\Kernel\Router;

class BramusRouter
{

    protected $afterRoutes = array();
    protected $beforeRoutes = array();
    protected $notFoundCallback = []; // Функция, которая будет выполнена, если ни один маршрут не сопоставлен
    protected $baseRoute = ''; // Текущий базовый маршрут, используемый для монтажа (под) маршрута.
    protected $serverBasePath; // Базовый путь сервера для выполнения маршрутизатора
    protected $requestedMethod = '';
    protected $namespace = '';

    public function before($methods, $pattern, $fn)
    {
        $pattern = $this->baseRoute . '/' . trim($pattern, '/');
        $pattern = $this->baseRoute ? rtrim($pattern, '/') : $pattern;

        foreach (explode('|', $methods) as $method) {
            $this->beforeRoutes[$method][] = array(
                'pattern' => $pattern,
                'fn' => $fn,
            );
        }
    }

    public function match($methods, $pattern, $fn)
    {
        $pattern = $this->baseRoute . '/' . trim($pattern, '/');
        $pattern = $this->baseRoute ? rtrim($pattern, '/') : $pattern;

        foreach (explode('|', $methods) as $method) {
            $this->afterRoutes[$method][] = array(
                'pattern' => $pattern,
                'fn' => $fn,
            );
        }
    }

    public function all($pattern, $fn)
    {
        $this->match('GET|POST|PUT|DELETE|OPTIONS|PATCH|HEAD', $pattern, $fn);
    }

    public function get(string $pattern, $fn)
    {
        $this->match('GET', $pattern, $fn);
    }

    public function post($pattern, $fn)
    {
        $this->match('POST', $pattern, $fn);
    }

    public function patch($pattern, $fn)
    {
        $this->match('PATCH', $pattern, $fn);
    }

    public function delete($pattern, $fn)
    {
        $this->match('DELETE', $pattern, $fn);
    }

    public function put($pattern, $fn)
    {
        $this->match('PUT', $pattern, $fn);
    }

    public function options($pattern, $fn)
    {
        $this->match('OPTIONS', $pattern, $fn);
    }

    public function mount($baseRoute, $fn)
    {
        $curBaseRoute = $this->baseRoute; // Track current base route
        $this->baseRoute .= $baseRoute;   // Build new base route string
        call_user_func($fn);              // Call the callable
        $this->baseRoute = $curBaseRoute; // Restore original base route
    }

    public function getRequestHeaders()
    {
        $headers = array();

        if (function_exists('getallheaders')) {
            $headers = getallheaders();
            if ($headers !== false) return $headers;
        }

        foreach ($_SERVER as $name => $value) {
            if ((substr($name, 0, 5) == 'HTTP_') || ($name == 'CONTENT_TYPE') || ($name == 'CONTENT_LENGTH')) {
                $headers[str_replace(array(' ', 'Http'), array('-', 'HTTP'), ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }

        return $headers;
    }

    public function setNamespace($namespace)
    {
        if (is_string($namespace)) $this->namespace = $namespace;
    }

    public function getNamespace()
    {
        return $this->namespace;
    }


    public function getRequestMethod()
    {

        $method = $_SERVER['REQUEST_METHOD']; // Take the method as found in $_SERVER
        if ($_SERVER['REQUEST_METHOD'] == 'HEAD') {
            ob_start();
            $method = 'GET';
        } elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $headers = $this->getRequestHeaders();
            if (isset($headers['X-HTTP-Method-Override']) && in_array($headers['X-HTTP-Method-Override'], array('PUT', 'DELETE', 'PATCH'))) {
                $method = $headers['X-HTTP-Method-Override'];
            }
        }
        return $method;
    }


    public function run($callback = null)
    {
        $this->requestedMethod = $this->getRequestMethod(); // Method name

        // Обработка перед выполнением основного запроса
        if (isset($this->beforeRoutes[$this->requestedMethod])) {
            $this->handle($this->beforeRoutes[$this->requestedMethod]);
        }

        $numHandled = 0;
        // Обработка основного маршрута
        if (isset($this->afterRoutes[$this->requestedMethod])) {
            $numHandled = $this->handle($this->afterRoutes[$this->requestedMethod], true);
            if(!empty($numHandled['route'])) {
                return $numHandled;
            }
        }


        if ($numHandled === 0) {  //  Если ни один маршрут не был обработан, вызовите ошибку 404 (если есть)
            $this->trigger404($this->afterRoutes[$this->requestedMethod]);
        } else {  // Если основной маршрут был обработан, выполните обработчик после запроса
            if ($callback && is_callable($callback)) {
                $callback();
            }
        }

        if ($_SERVER['REQUEST_METHOD'] == 'HEAD') { // Если изначально это был запрос HEAD, очистите за собой, очистив выходной буфер
            ob_end_clean();
        }

        return $numHandled !== 0;
    }

    private function handle($routes, $quitAfterRun = false)
    {

        $numHandled = 0;
        $uri = $this->getCurrentUri();

        foreach ($routes as $route) {
            $is_match = $this->patternMatches($route['pattern'], $uri, $matches, PREG_OFFSET_CAPTURE);
            if ($is_match) {
                $matches = array_slice($matches, 1);
                $params = array_map(function ($match, $index) use ($matches) {
                    if (isset($matches[$index + 1]) && isset($matches[$index + 1][0]) && is_array($matches[$index + 1][0])) {
                        if ($matches[$index + 1][0][1] > -1) {
                            return trim(substr($match[0][0], 0, $matches[$index + 1][0][1] - $match[0][1]), '/');
                        }
                    }
                    return isset($match[0][0]) && $match[0][1] != -1 ? trim($match[0][0], '/') : null;
                }, $matches, array_keys($matches));

                ++$numHandled;

                $this->invoke($route['fn'], $params);
                if ($quitAfterRun) break;
//                if ($quitAfterRun) return ['route'  => $route, 'params' => $params,];
//                else $this->invoke($route['fn'], $params);
            }
        }

        return $numHandled;
    }

    protected function invoke($fn, $params = array())
    {

        if (is_callable($fn)) {
            call_user_func_array($fn, $params);
        } elseif (stripos($fn, '@') !== false) {

            list($controller, $method) = explode('@', $fn);
            if ($this->getNamespace() !== '') {
                $controller = $this->getNamespace() . '\\' . $controller;
            }

            try {
                $reflectedMethod = new \ReflectionMethod($controller, $method);
                if ($reflectedMethod->isPublic() && (!$reflectedMethod->isAbstract())) {
                    if ($reflectedMethod->isStatic()) {
                        forward_static_call_array(array($controller, $method), $params);
                    } else {
                        if (\is_string($controller)) {
                            $controller = new $controller();
                        }
                        call_user_func_array(array($controller, $method), $params);
                    }
                }
            } catch (\ReflectionException $reflectionException) {
                // The controller class is not available or the class does not have the method $method
            }
        }
    }

    public function getCurrentUri()
    {
        $basePath = $this->getBasePath();
        $uri = substr(rawurldecode($_SERVER['REQUEST_URI']), strlen($basePath));
        if (strstr($uri, '?')) {
            $uri = substr($uri, 0, strpos($uri, '?'));
        }
        return '/' . trim($uri, '/');
    }

    public function getBasePath()
    {
        if ($this->serverBasePath === null) {
            $scriptName = explode('/', $_SERVER['SCRIPT_NAME']);
            $slice = array_slice($scriptName, 0, -1);
            $this->serverBasePath = implode('/', $slice) . '/';
        }
        return $this->serverBasePath;
    }

    public function setBasePath($serverBasePath)
    {
        $this->serverBasePath = $serverBasePath;
    }

    public function set404($match_fn, $fn = null)
    {
        if (!is_null($fn)) $this->notFoundCallback[$match_fn] = $fn;
        else $this->notFoundCallback['/'] = $match_fn;
    }

    public function trigger404($match = null)
    {
        $numHandled = 0;
        if (count($this->notFoundCallback) > 0)
        {
            foreach ($this->notFoundCallback as $route_pattern => $route_callable) {
                $matches = [];
                $is_match = $this->patternMatches($route_pattern, $this->getCurrentUri(), $matches, PREG_OFFSET_CAPTURE);
                if ($is_match) {
                    $matches = array_slice($matches, 1);
                    $params = array_map(function ($match, $index) use ($matches) {
                        if (isset($matches[$index + 1]) && isset($matches[$index + 1][0]) && is_array($matches[$index + 1][0])) {
                            if ($matches[$index + 1][0][1] > -1) {
                                return trim(substr($match[0][0], 0, $matches[$index + 1][0][1] - $match[0][1]), '/');
                            }
                        }
                        return isset($match[0][0]) && $match[0][1] != -1 ? trim($match[0][0], '/') : null;
                    }, $matches, array_keys($matches));
                    $this->invoke($route_callable);
                    ++$numHandled;
                }
            }
        }

        if (($numHandled == 0) && (isset($this->notFoundCallback['/']))) {
            $this->invoke($this->notFoundCallback['/']);
        } elseif ($numHandled == 0) {
            header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
        }
    }


    private function patternMatches($pattern, $uri, &$matches, $flags)
    {
        $pattern = preg_replace('/\/{(.*?)}/', '/(.*?)', $pattern);
        return boolval(preg_match_all('#^' . $pattern . '$#', $uri, $matches, PREG_OFFSET_CAPTURE));
    }

}