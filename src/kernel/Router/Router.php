<?php

namespace Dzion\Kernel\Router;

use Dzion\Kernel\AppContainer;
use Dzion\Kernel\Interfaces\ContainerInterface;
use Dzion\Kernel\Interfaces\RouterInterface;

class Router extends BramusRouter implements RouterInterface
{

    protected ContainerInterface $container;

    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function middleware($methods, $pattern, $fn)
    {

        $methods = explode('|', $methods);

        if(is_array($pattern)) {

            $patternList = $pattern;

            foreach ($methods as $method) {
                foreach ($patternList as $pattern) {

                    $pattern = $this->baseRoute . '/' . trim($pattern, '/');
                    $pattern = $this->baseRoute ? rtrim($pattern, '/') : $pattern;

                    $this->beforeRoutes[$method][] = array(
                        'pattern' => $pattern,
                        'fn' => $fn,
                    );
                }
            }

        }  else {

            $pattern = $this->baseRoute . '/' . trim($pattern, '/');
            $pattern = $this->baseRoute ? rtrim($pattern, '/') : $pattern;

            foreach ($methods as $method) {
                $this->beforeRoutes[$method][] = array(
                    'pattern' => $pattern,
                    'fn' => $fn,
                );
            }
        }

    }

    public function group($baseRoute, $fn)
    {
        $curBaseRoute = $this->baseRoute; // Track current base route
        $this->baseRoute .= $baseRoute;   // Build new base route string
        call_user_func($fn);              // Call the callable
        $this->baseRoute = $curBaseRoute; // Restore original base route
    }

    protected function invoke($fn, $params = [])
    {

        if (is_callable($fn)) {
            call_user_func_array($fn, $params);
        } elseif (is_array($fn)) {

            $controller = $fn[0];
            $method = $fn[1];
            $this->controllerHandle($controller, $method, $params);

        } elseif (stripos($fn, '@') !== false) {

            list($controller, $method) = explode('@', $fn);
            $this->controllerHandle($controller, $method, $params);

        }
    }

    protected function controllerHandle($controller, $method, $params) {
        $this->container->get($controller, $params, $method);
    }

    public function setRoutes(array $routes = [])
    {
        foreach ($routes as $method => $list) {
            if(empty($list)) continue;

            foreach ($list as $url => $route) {
                $this->$method($url, $route);
            }
        }
    }

}