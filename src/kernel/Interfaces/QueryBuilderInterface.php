<?php

namespace Dzion\Kernel\Interfaces;

interface QueryBuilderInterface
{
    public function from(string $table, ?string $alias = null): self;
}