<?php

namespace Dzion\Kernel\Interfaces;

interface TemplateInterface
{
    public function render(string $file, array $data = []) : self;
}