<?php

namespace Dzion\Kernel\Interfaces;

interface ModelInterface
{
    public function insert(array $data, string $table = '');
}