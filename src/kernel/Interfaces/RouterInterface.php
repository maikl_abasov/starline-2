<?php

namespace Dzion\Kernel\Interfaces;

interface RouterInterface
{
    public function get(string $url, $fn);

}