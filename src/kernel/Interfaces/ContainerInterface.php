<?php

namespace Dzion\Kernel\Interfaces;

interface ContainerInterface
{
    public static function getInstance(array $container = []): self;
}