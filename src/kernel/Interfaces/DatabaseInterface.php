<?php

namespace Dzion\Kernel\Interfaces;

interface DatabaseInterface
{
    public function query(string $sql, array $params = []);

}