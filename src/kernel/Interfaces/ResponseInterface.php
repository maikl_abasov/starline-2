<?php

namespace Dzion\Kernel\Interfaces;

interface ResponseInterface
{
    public function getContent();
}