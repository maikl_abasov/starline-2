<?php

namespace Dzion\Kernel\Interfaces;

interface RequestInterface
{
    public static function createFromGlobals(): static;
    public function getRequestUri(): string;
}