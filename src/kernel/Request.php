<?php

namespace Dzion\Kernel;

class Request
{

    private array $get;
    private array $post;
    private array $cookie;
    private array $files;
    private array $server;
    private array $request;
    private string $requestUri;

    public function __construct(
        array $get,
        array $postData,
        array $cookie,
        array $files,
        array $server,
        array $request,
    )
    {

        $this->get = $get;
        $this->post = $postData;
        $this->cookie = $cookie;
        $this->files = $files;
        $this->server = $server;
        $this->request = $request;

        $this->ajaxData();
        $this->setRequestUri();
    }

    public static function createFromGlobals(): static
    {
        return new static($_GET, $_POST, $_COOKIE, $_FILES, $_SERVER, $_REQUEST);
    }

    protected function setRequestUri(): void
    {
        $uri = $this->getParam('server', 'REQUEST_URI');
        $script = $this->getParam('server', 'SCRIPT_NAME');

        $uriList = explode('/', trim($uri, '/'));
        $scriptList = explode('/', trim($script, '/'));
        array_pop($scriptList);
        foreach ($scriptList as $key => $value) {
            $index = array_search($value, $uriList);
            if ($index !== false) {
                unset($uriList[$index]);
            }
        }
        $this->requestUri = implode('/', $uriList);
    }

    public function getRequestUri(): string
    {
        return $this->requestUri;
    }

    public function getParam(string $arrName, string $key = ''): mixed
    {
        $result = null;
        if(!empty($this->$arrName)) {
            $result = (!empty($this->$arrName[$key])) ? $this->$arrName[$key]: $this->$arrName;
        }
        return $result;
    }

    public function getMethod(): string
    {
        return $this->getParam('server', 'REQUEST_METHOD');
    }

    protected function ajaxData() : void {
        $json = file_get_contents('php://input');
        $post = json_decode($json, true);
        if((!empty($post))) {
            $post = (array)$post;
            foreach ($post as $name => $item) $this->post[$name] = $item;
        }
    }

    public function get($key){
        return $this->getParam($this->get, $key);
    }

    public function post($key){
        return $this->getParam($this->post, $key);
    }

    public function server($key){
        return $this->getParam($this->server, $key);
    }

}