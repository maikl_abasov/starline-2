<?php

namespace Dzion\Kernel;

use Dzion\Kernel\Interfaces\ContainerInterface;

class AppContainer implements ContainerInterface
{

    private static $instance = null;

    protected array $container = [];

    public static function getInstance(array $container = []): self {
        if(!self::$instance) {
            self::$instance = new self($container);
        }
        return self::$instance;
    }

    private function __construct(array $container) {
        $this->setContainer($container);
    }

    public function set(string $key, mixed $object = NULL) : void
    {
        if ($object === NULL) $object = $key;
        $this->container[$key] = $object;
    }

    public function has(string $key) : bool
    {
        return (!isset($this->container[$key])) ? false : true;
    }

    public function get(string $key, array $params = [], $method = null)
    {
        if (!isset($this->container[$key])) $this->set($key);
        $object = $this->container[$key];
        return $this->resolve($object, $params, $method);
    }

    public function setContainer(array $container) : void
    {
        $this->setParams($container);
    }

    public function getContainer(string | int $key = null) : array
    {
        return (!empty($this->container[$key])) ? $this->container[$key] : $this->container;
    }

    protected function resolve($object, $params = null, string $method = null)
    {

        if(!empty($params)) $this->setParams($params);

        if ($object instanceof \Closure) {  // если это функция
            return $object($this, $params);
        }

        $objectType = gettype($object);

        if($objectType == 'array' || $objectType == 'object' || (!class_exists($object))) {
           return $object;  // если это примитивы (array, string, объект уже создан)
        }

        // -------------
        $reflector = new \ReflectionClass($object);

        if (!$reflector->isInstantiable()) {  // Проверяет, можно ли создать экземпляр класса
            throw new \Exception('Невозможно создать объект класс: "' . $object . '"!');
        }

        $constructReflector = $reflector->getConstructor();
        if (empty($constructReflector)) { // если нет конструктора
            $resolvedClass = new $object();
            if($method)  return $this->makeMethod($resolvedClass, $method);
            return $resolvedClass;
        }

        $constructArguments = $constructReflector->getParameters();

        if (empty($constructArguments)) { // если нет аргументов конструктора
            $resolvedClass = new $object();
            if($method)  return $this->makeMethod($resolvedClass, $method);
            return $resolvedClass;
        }

        $dependencies = $this->getDependencies($constructArguments);
        $resolvedClass = new $object(...$dependencies);

        if($method) return $this->makeMethod($resolvedClass, $method);

        return $resolvedClass;
    }

    protected function getDependencies($constructArguments)
    {
        $dependencies = [];

        foreach ($constructArguments as $argument) {

            $name = $argument->getName();
            $type = $argument->getType();

            if($type instanceof \ReflectionNamedType) {
                $objName = $argument->getType()->getName();
                if (!empty($this->container[$objName])) $object = $objName; // если интерфейс
                else  $object = (!class_exists($objName)) ? $name : $objName;
                $data = $this->get($object);
            } else {
                $data = $this->get($name);
            }

            $dependencies[$name] = $data;
        }

        return $dependencies;
    }

    protected function setParams(array $params = []) {
        if(empty($params)) return false;

        foreach ($params as $key => $param) {
            $this->set($key, $param);
        }
    }

    protected function makeMethod($class, $method)
    {
        if (!method_exists($class, $method)) {
            throw new \Exception("Не найден метод класса: '{$method}'!");
        }

        $reflectionMethod = new \ReflectionMethod($class, $method);
        $parameters = $reflectionMethod->getParameters();
        $dependencies = $this->getDependencies($parameters);
        $response = $reflectionMethod->invokeArgs($class, $dependencies);

        return $response;
    }

}