<?php

namespace Dzion\Kernel\Database;

use PDO;
use Dzion\Kernel\Interfaces\QueryBuilderInterface;
use Dzion\Kernel\Interfaces\DatabaseInterface;
use Dzion\Kernel\Interfaces\ModelInterface;

class Model implements ModelInterface
{
    protected PDO $pdo;
    protected DatabaseInterface $db;
    protected QueryBuilderInterface $builder;
    protected string $table;
    protected string $primaryKey;
    protected string $query = '';
    protected array $params;
    protected \PDOStatement $stmt;

    public function __construct() {
        $this->db = app(DatabaseInterface::class);
        $this->builder = app(QueryBuilderInterface::class);
        $this->pdo = $this->db->getPdo();
    }

    public function setDatabase($host, $driver, $user, $pwd, $dbname, $port) {
        $CONFIG = getEnvFile();
        $conf = [
            'host'   => getConfig($CONFIG, $host),
            'driver' => getConfig($CONFIG, $driver),
            'user'   => getConfig($CONFIG, $user),
            'password' => getConfig($CONFIG, $pwd),
            'dbname' => getConfig($CONFIG, $dbname),
            'port'   => getConfig($CONFIG, $port),
        ];
        $this->db = new Database($conf);
    }

    public function createQuery(array $params = [], string | null $table = null, string $fields = '*') :self {
        $table = (!$table) ? $this->table : $table;
        $this->query = "SELECT {$fields} FROM {$table} " . $this->prepareValues($params);
        return $this;
    }

    public function make(string $query = '', array $params = []) : self {
        $query  = ($query) ? $this->query . $query : $this->query;
        $params = (!empty($params)) ? $params : $this->params;
        try {
            $this->stmt = $this->db->getPdo()->prepare($query);
            $this->stmt->execute($params);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
        return $this;
    }

    public function get(bool $first = false) {
        if($first)  return $this->stmt->fetch(\PDO::FETCH_ASSOC);
        return $this->stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function select(array $params = [], string $table = '', string $sql = '', string $fields = '*') : array {
        $table = (!$table) ? $this->table : $table;
        $sql   = "SELECT {$fields} FROM {$table} " . $this->prepareValues($params) . $sql;
        $stmt = $this->db->getPdo()->prepare($sql);
        $stmt->execute($this->params);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function first(array $params = [], string $table = '', string $sql = '', string $fields = '*') : array {
        $table = (!$table) ? $this->table : $table;
        $sql   = "SELECT {$fields} FROM {$table} " . $this->prepareValues($params) . $sql;
        $stmt = $this->db->getPdo()->prepare($sql);
        $stmt->execute($this->params);
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function prepareValues(array $params = []) : string {
        $fields = $values = [];
        foreach ($params as $name => $value) {
            if(is_string($name)) {
                $fields[] = "$name = :$name";
                $values[$name] = $value;
            } elseif(is_array($value)) {
                $fname  = $value[0];
                if(!empty($value[2])) {
                    $operation = $value[1];
                    $fields[] = " $fname $operation :$fname ";
                    $values[$fname] = $value[2];
                } else {
                    $fields[] = " $fname = :$fname ";
                    $values[$fname] = $value[1];
                }
            }
        }

        $this->params = $values;

        return (!empty($fields)) ? ' WHERE ' . implode(',', $fields) : '';
    }

    public function insert(array $data, string $table = '') : int | bool {
        $table = (!$table) ? $this->table : $table;
        return $this->db->insert($data, $table);
    }

    public function update(array $data, array $where = [], string $table = '') : int | bool {
        $table = (!$table) ? $this->table : $table;
        return $this->db->update($data, $table, $where);
    }

    public function query(string $sql, array $params = []) : array | object {
        return $this->db->query($sql, $params);
    }

    public function find(int $id, string $table = '') : array | object {
        $table = (!$table) ? $this->table : $table;
        $primaryKey = (!$this->primaryKey) ? 'id' : $this->primaryKey;
        $sql = "SELECT * FROM {$table} WHERE {$primaryKey}=:{$primaryKey} LIMIT 1";
        return $this->db->query($sql, [$primaryKey => $id]);
    }

    public function searchItems(array $data): array {
        $table = $data['table'];
        $field = $data['field'];
        $value = $data['value'];
        $limit = (!empty($data['limit'])) ? " LIMIT " . $data['limit'] : "";
        $result = $this->query("SELECT * FROM $table WHERE $field LIKE '%$value%' " . $limit);
        return $result;
    }

}