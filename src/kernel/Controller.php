<?php

namespace Dzion\Kernel;

use Dzion\App\Models\Main;
use Dzion\Kernel\Database\Model;

abstract class Controller
{

    protected $templatePath;

    protected AppContainer $app;

    public function __construct()
    {
        $this->templatePath = TEMPLATE_PATH;
        $this->app = AppContainer::getInstance();
    }

    public function post() : array
    {
        $data = file_get_contents('php://input');
        return (array)json_decode($data);
    }

    public function redirect(string $page) :void
    {

        $list = explode('/', trim($_SERVER['REQUEST_URI'], '/'));

        $line = [];
        foreach ($list as $value) {
            if($value == 'public') break;
            $line[] = $value;
        }

        if(!empty($line)) {
            $url = '/' . implode('/', $line) . '/public' . $page;
        } else {
            $url = '/public' . $page;;
        }

        header("Location: " . $url);

        exit;
    }

    public function render(string $file, array $data = [])
    {
        $template = new Template($this->templatePath, 'index');
        $html = $template->render($file, $data)->getHtml();
        $response = new Response($html);
        return $response->html()->getContent();
    }

    public function json(array $data)
    {
        $response = new Response($data);
        return $response->json()->getContent();
    }

    public function searchItems(array | string $data = []) {
        $post = (empty($data)) ? $data : $this->post();
        $result = (new Main())->searchItems($post);
        return $this->json($result);
    }

}