<?php

namespace Dzion\Kernel;

use Dzion\Kernel\Interfaces\ContainerInterface;
use Dzion\Kernel\Interfaces\RouterInterface;

class AppKernel
{
    protected ContainerInterface $container;

    protected RouterInterface $router;

    public function __construct(RouterInterface $router, ContainerInterface $container)
    {
        $this->container = $container;
        $this->router = $router;
        $this->router->setContainer($this->container);
    }

    public function handle() {

        $route = $this->router->run();
        // $this->run($route);

    }

    protected function run($route) {

        if(empty($route['route'])) {
            throw new Exception('Не найден маршрут');
        }

        $params = (!empty($route['params'])) ? $route['params'] : [];
        $route = $route['route'];

        if(!empty($route['fn'])) {

            $fn = $route['fn'];

            if (is_callable($fn)) {
                call_user_func_array($fn, $params);
            } elseif (is_array($fn)) {

                $controller = $fn[0];
                $method = $fn[1];
                $this->controllerHandle($controller, $method, $params);

            } elseif (stripos($fn, '@') !== false) {

                list($controller, $method) = explode('@', $fn);
                $this->controllerHandle($controller, $method, $params);
            }
        }

    }

    protected function controllerHandle($controller, $method, $params) {
        $this->container->get($controller, $params, $method);
    }

}