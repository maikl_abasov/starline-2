<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

set_exception_handler('customExceptionHandler');
set_error_handler("customErrorHandler");

// trigger_error("Не могу поделить на ноль", E_USER_WARNING);

//////////////////////////////////////
///
///
///
function saveLog($data, $fileName = '', $ext = 'log')
{
    $data['REQUEST_URI'] = $_SERVER['REQUEST_URI'];
    $data = print_r($data, true);
    $datetime = date('d.m.Y h:i:s');

    $logData = "================= START ================== \n";
    $logData .= "Datetime:  $datetime \n";
    $logData .= "$data \n";
    $logData .= "================= END ===================== \n\n";

    $logFile = LOG_PATH . '/' . $fileName . '_error.' . $ext;
    if (!file_exists(LOG_PATH)) mkdir(LOG_PATH, 0755);
    if (file_exists($logFile)) $logData .= file_get_contents($logFile);
    $save = file_put_contents($logFile, $logData);
}

function customExceptionHandler(\Throwable $exception)
{
    $errorFormat = exceptionFormatter($exception);
    $errorFormat['TITLE'] =  "Неперехваченное исключение";
    saveLog($errorFormat, 'exception');
}

// функция обработки ошибок
function customErrorHandler($errno, $errText, $errfile, $errline)
{
    if (!(error_reporting() & $errno)) return false;

    $errText = htmlspecialchars($errText);
    $message = '';

    switch ($errno) {
        case E_USER_ERROR:
            $message .= "ПОЛЬЗОВАТЕЛЬСКАЯ ОШИБКА: [$errno] $errText \n";
            $message .= "ФАЙЛ: $errfile \n";
            $message .= "СТРОКА В ФАЙЛЕ: $errline \n";
            $message .= "PHP " . PHP_VERSION . " | (" . PHP_OS . ") | Завершение работы.\n";
            break;

        case E_USER_WARNING:
            $message .= "ПОЛЬЗОВАТЕЛЬСКОЕ ПРЕДУПРЕЖДЕНИЕ: [$errno] $errText \n";
            break;

        case E_USER_NOTICE:
            $message .= "ПОЛЬЗОВАТЕЛЬСКОЕ УВЕДОМЛЕНИЕ: [$errno] $errText \n";
            break;

        default:
            $message .= "НЕИЗВЕСТНАЯ ОШИБКА: [$errno] $errText \n";
            break;
    }

    saveLog($message, 'fatal');

    /* Не запускаем внутренний обработчик ошибок PHP */
    return true;
}

function exceptionFormatter(\Throwable $exception) : array
{

    $tracer = [];
    $trace = $exception->getTrace();
    // lg($trace);
    for ($i = 0; $i < 2; $i++) {
        if(!empty($trace[$i])) $tracer[] = $trace[$i];
    }

    return [
        'datetime' => date('d.m.Y h:i:s'),
        'message'  => $exception->getMessage(),
        'file'     => $exception->getFile(),
        'line'     => $exception->getLine(),
        'code'     => $exception->getCode(),
        'tracer'    => (!empty($tracer)) ? print_r($tracer, true) : '',
    ];
}

function errorLastProcess() {
    $errorLast = error_get_last();

    if(is_array($errorLast) && in_array($errorLast['type'], [E_ERROR, E_PARSE, E_CORE_ERROR, E_COMPILE_ERROR])) {

    }
}

