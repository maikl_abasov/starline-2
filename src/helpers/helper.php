<?php

function app($key = null, $params = []) {
    $container = \Dzion\Kernel\AppContainer::getInstance();
    return ($key) ? $container->get($key, $params) : $container;
}

function getEnvFile(string $key = '') : array {
    $env = file(APP_PATH . '/.env');
    $config = [];

    foreach ($env as $i => $line) {
        $line = trim($line);
        if(!$line) continue;

        $item = explode('=', $line);
        $fname = trim($item[0]);
        $value = trim($item[1]);
        $config[$fname] = $value;
    }
    return (!empty($config[$key])) ? $config[$key] : $config;
}

function getConfig(array $config, string $key = '', $default = null) : array | string | null {
    return (!empty($config[$key])) ? $config[$key] : $default;
}

function loadFile(string $filePath, bool $loadType = false) {
    $path = APP_PATH . $filePath;
    if($loadType) return require $path;
    require $path;
}

function getErrorResponse(\Exception $exception, $type, $title = '') {

    $error = [
        'type' => $type,
        'message' => $exception->getMessage(),
        'file' => $exception->getFile(),
        'line' => $exception->getLine(),
        'code' => $exception->getCode(),
        'trace' => $exception->getTraceAsString(),
    ];

    if($title) $error['title'] = $title;

    die("<pre>" .print_r($error, true). "</pre>");

}

function getSiteUri() {

    $list = explode('/', trim($_SERVER['REQUEST_URI'], '/'));

    $line = [];
    foreach ($list as $value) {
        if($value == 'public') break;
        $line[] = $value;
    }

    $url = '/';

    if(!empty($line)) {
        $url = '/' . implode('/', $line);
    }

    return $url;
}


function lg() {
    $args = func_get_args();
    $data = print_r($args, true);
    die("<pre>$data</pre>");
}



