<?php

namespace Dzion\App\Services;

use Dzion\App\Models\Queue;

class QueueInitService
{
    protected $eventClass;
    protected $data;
    protected $model;

    public function __construct($eventClass, $data)
    {
        $this->eventClass = $eventClass;
        $this->data       = $data;
        $this->model      = new Queue();
    }

    public function createQueue()
    {
        $this->model->insert([
            'class' => $this->eventClass,
            'data'  => $this->data,
        ], 'queues');
    }

}