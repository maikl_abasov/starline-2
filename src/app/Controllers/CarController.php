<?php

namespace Dzion\App\Controllers;

use Dzion\App\Models\Car;
use Dzion\Kernel\Controller;
use Dzion\Kernel\Interfaces\ModelInterface;

class CarController extends Controller
{
    protected ModelInterface $model;

    public function __construct(Car $model)
    {
        parent::__construct();
        $this->model = $model;
    }

    public function index()
    {
        return $this->render('car', []);
    }

    public function getCarItems($form = [])
    {
        $form = (!empty($form)) ? $form : $this->post();

        $data = $this->model->getCarItems($form);

        return $this->json($data);
    }

}