<?php

namespace Dzion\App\Controllers;

use Dzion\App\Models\Main;
use Dzion\Kernel\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $data = ['message' => 'home->index'];
        return $this->render('home', $data);
    }

    public function hello($name, $age) {
        return $this->json(['name' => $name, 'age' => $age]);
    }

}