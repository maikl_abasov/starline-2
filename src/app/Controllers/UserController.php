<?php

namespace Dzion\App\Controllers;

use Dzion\App\Models\User;
use Dzion\Kernel\Controller;
use Dzion\Kernel\Interfaces\ModelInterface;

class UserController extends  Controller
{

    protected ModelInterface $model;

    public function __construct(User $model)
    {
        parent::__construct();
        $this->model = $model;
    }

    public function index()
    {
        // $cars = $this->model->query('SELECT * FROM cars');
        $data = ['users' => ['Maikl', 'Serg']];
        return $this->render('user', $data);
    }

}