<?php

namespace Dzion\App\Controllers\Auth;

use Dzion\Kernel\Controller;
use Dzion\Kernel\Interfaces\ModelInterface;

class AuthController extends Controller
{

    protected ModelInterface $user;
    public const AUTH_NAME = 'auth_state';

    public function __construct(ModelInterface $user)
    {
        parent::__construct();
        $this->user = $user;
    }

    public function auth(array $data) : bool
    {

        $status = false;
        $email = $data['email'];
        $user = $this->user->createQuery([
           ['email', '=', $email]
        ], 'users')->make()->get(true);
        // $user = $this->user->first(['email' => $email], 'users');

        if(!empty($user)) {
            $status = password_verify($data['password'], $user['password']);
            if($status) {
                setcookie(self::AUTH_NAME, $data['password'] , time()+6600);
            }
        }

        return $status;
    }


    public function register(array $data) : bool | int
    {
        $password = $data['password'];
        $confirmPassword = $data['confirm_password'];
        if($confirmPassword != $password) return false;

        unset($data['confirm_password']);
        $data['password'] = password_hash($password, PASSWORD_DEFAULT);
        $userId = $this->user->insert($data, 'users');
        return $userId;
    }

    public function check()
    {
        if(empty($_COOKIE[self::AUTH_NAME])) {
            $this->redirect('/login-form');
        }
    }

    public function logout()
    {
        setcookie(self::AUTH_NAME, "", time()- 60);
    }

}