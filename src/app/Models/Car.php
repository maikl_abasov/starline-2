<?php


namespace Dzion\App\Models;

use Dzion\Kernel\Database\Model;

class Car extends Model
{

    public function __construct() {
        parent::__construct();
    }

    public function getCarItems(array $form) : array {

        // $form = $this->getTestFormData();

        $filters = $this->queryFilterHandle($form);
        $condition = $filters['condition'];
        $limit = $filters['limit'];
        $orderBy = $filters['order_by'];

//        if(!$condition) $condition = " WHERE generation.name IS NOT NULL";
//        else $condition = $condition . "  AND generation.name IS NOT NULL";

        $fields = "
            car.*, 
            mark.name  AS mark_name,
            mark.id    AS mark_id,
            model.name AS model_name,
            model.id   AS model_id,

            generation.name   AS generation_name,
            generation.id     AS generation_id,

            modification.name AS modification_name,
            modification.id   AS modification_id,
            modification.body_type,
            modification.fuel_type,
            modification.drive_type,
            modification.transmission,
            modification.power,
            modification.engine_size,

            city.city AS city_name,
            city.city_id
        ";

        $joinList = "
            LEFT JOIN geo_cities        AS city         ON car.city = city.city_id             
            LEFT JOIN avc_marks         AS mark         ON car.mark_id = mark.id
            LEFT JOIN avc_models        AS model        ON car.model_id = model.id
            LEFT JOIN avc_generations   AS generation   ON car.generation_id = generation.id
            LEFT JOIN avc_modifications AS modification ON car.modification_id = modification.id
        ";

        $from = " FROM cars AS car $joinList  $condition ";
        $sql  = "SELECT $fields " . $from . $orderBy . $limit;
        $cars = $this->query($sql);
        $count = $this->query("SELECT count(car.id) " . $from);
        $count = (!empty($count[0]['count(car.id)'])) ? $count[0]['count(car.id)'] : 0;

        return [
           'count' => $count,
           'list'  => $cars,
           'sql'   => $sql,
        ];
    }

    protected function queryFilterHandle($form) {

        $filters = [];

        // ----- like (name LIKE "%value%") -----------
        foreach ($form['like'] as $name => $value) {
            if (empty($value)) continue;
            if(is_string($value)) {
                $filters[] = "$name LIKE '%$value%'";
            }
        }
        // -----./ like (name LIKE "%value%") ----------

        // ---- between (2011 BETWEEN 2013)  -----------
        foreach ($form['between'] as $name => $item) {
            if (empty($item)) continue;

            $between = [];
            $item = (array)$item;
            foreach ($item as $param) {
                $param = (array)$param;
                if(!empty($param['with']) && !empty($param['by'])) {
                    $between[] = " ($name BETWEEN " .$param['with'].  " AND " . $param['by'] . ") ";
                }
            }

            if(!empty($between)) {
                $filters[] = " (" . implode(' OR ', $between) . ") ";
            }
        }
        // ----./ between (2011 BETWEEN 2013) --------

        // ---- in (AND (name = value OR name=value2 ))  ------
        $in = [];
        foreach ($form['in'] as $name => $item) {
            if (empty($item)) continue;
            $item = (array)$item;
            foreach ($item as  $value) {
                $in[] = " $name = '$value' ";
            }

        }

        if(!empty($in)) {
            $filters[] = " (" . implode(' OR ', $in) . ") ";
        }
        // ----./ in (AND (name = value OR name=value2 )) ------

        $condition = (!empty($filters)) ? " WHERE " . implode(' AND ', $filters) : '';

        //---- ODER BY ---------------------
        $sort = [];
        $orderBy = '';
        foreach ($form['sort'] as $name => $value) {
            if (empty($value)) continue;
            $sort[] = " $name $value ";
        }
        if(!empty($sort)) {
            $orderBy = " ORDER BY " . implode(',', $sort);
        }
        //----./ ORDER BY -----------------

        // ---- limit (4, 10) -------------
        $limit = '';
        if(!empty($form['page'])) {
            $formPage = (array)$form['page'];
            $pageNum = $formPage['page_num'];
            $limit = $formPage['limit'];
            $offset = ($pageNum - 1) * $limit;
            $limit = (isset($offset) & isset($limit)) ? " LIMIT $offset, $limit" : '';
        }
        // ----./  limit ----------------

        return [
           'condition' => $condition,
           'order_by'  => $orderBy,
           'limit'     => $limit,
        ];
    }
}