<?php

session_start();

define('APP_START', time());
define('APP_PATH', dirname(__DIR__));
define('LOG_PATH', APP_PATH . '/logs');

require_once APP_PATH . '/src/helpers/helper.php';
require_once APP_PATH . '/src/helpers/errors_handle.php';
require_once APP_PATH . '/vendor/autoload.php';

$siteUri = getSiteUri();

define('METRONIC_URL', 'https://21090.aqq.ru/GLOBAL-ASSETS/metronic');
define('HOST', $_SERVER['REQUEST_SCHEME'] . '://'. $_SERVER['SERVER_NAME']);
define('SITE_URI', $siteUri);
define('TEMPLATE_PATH', APP_PATH . '/template');
define('TEMPLATE_URL', HOST. SITE_URI . '/template');

try {

    $config = require APP_PATH . '/config/config.php';

    $container = \Dzion\Kernel\AppContainer::getInstance($config);

    $router = require APP_PATH . '/config/routes.php';

    $app = new \Dzion\Kernel\AppKernel($router, $container);

    $app->handle();

} catch (Exception $exception) {

    getErrorResponse($exception, 'APP ERROR');

}


