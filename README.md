
## SIMPLE APP

### использование Router

class SimpleController {

   public function send($name, $age) {
     echo 'Меня зовут ' . $name . ', мне ' . $age . ' лет' ;
   }

}

###### Вариант 1
$router->get('/test-page/{name}/{age}', [SimpleController::class, "send"]);

###### Вариант 2
$router->get('/test-page/{name}/{age}', SimpleController::class . "@" . "send");

###### Вариант 3
$router->get('/test-page/{name}/{age}', function($name, $age) use ($database)  {

   echo 'меня зовут ' . $name . ', мне ' . $age . ' лет' ;

});


### Для запуска
создать базу и прописать в .env

composer install


## routes list




